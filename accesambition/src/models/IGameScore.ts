export interface IGameScore {
    id?: string;
    teamID: string;
    score: number;
    place: number;
}