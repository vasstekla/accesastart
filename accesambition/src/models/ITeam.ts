import { ITeamScore } from './ITeamScore';

export interface ITeam {
    id: string;
    name: string;
    players: string[];
    scores: ITeamScore[];
    isSingleUser: boolean;
}