import {IGameScore} from './IGameScore'

export interface IGame {
    id: string;
    gameDescription: string;
    date: Date;
    gameType: number;
    gameScore: IGameScore[];
    image: string;
}