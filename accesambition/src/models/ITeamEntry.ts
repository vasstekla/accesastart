import { ITeamScore } from './ITeamScore';

export interface ITeamEntry {
    name: string;
    players: string[];
    scores: ITeamScore[];
    isSingleUser: boolean;
}