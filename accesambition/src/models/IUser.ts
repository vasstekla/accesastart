export interface IUser {
    id?: string;
    firstName: string;
    lastName: string;
    username: string;
    emailAdress: string;
    password: string;
    token: string;
    profilePicture: string;
}