export interface ITeamScore {
    id?: string;
    gameType: number;
    score: number;
}