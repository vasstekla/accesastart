export interface IGameDetails {
    team: string;
    isSingleUser: boolean;
    score: number;
    place: number;
}