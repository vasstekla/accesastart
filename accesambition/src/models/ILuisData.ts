import { ILuisIntent } from './ILuisIntent';
import { ILuisEntity } from './ILuisEntity';

export interface ILuisData {
    query: string;
    topScoringIntent: ILuisIntent;
    intents: ILuisIntent[];
    entities: ILuisEntity[];
}