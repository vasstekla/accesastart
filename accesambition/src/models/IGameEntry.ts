import {IGameScore} from './IGameScore'

export interface IGameEntry {
    gameDescription: string;
    date: Date;
    gameType: number;
    gameScore: IGameScore[];
    image: string;
}