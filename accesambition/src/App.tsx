import * as React from 'react';
import './App.css';
import Root from './Root';

class App extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div className="App">
        <Root />
      </div>

    );
  }
}

export default App