import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './AllTeams.css';
import TeamService from '../../services/TeamService'
import UserService from '../../services/UserService'
import { ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails } from '@material-ui/core';
import { ITeam } from '../../models/ITeam'
import { IUser } from 'src/models/IUser';
import CircularProgress from '@material-ui/core/CircularProgress';
import AiBot from '../aiBot/aiBot';

export default class AllTeams extends React.Component<any, any> {

    private pageSize: number = 15;
    private pageNumber: number = 1;
    private hasMore: boolean = true;

    constructor(props: any) {
        super(props);

        this.state = {
            teams: [],
            player: '',
            teamname: '',
            fetching: false,
            scrollFetching: false,
        }
    }

    public renderTeamsWithPagination() {
        TeamService.getAllTeamsWithPagination(this.pageSize, this.pageNumber)
            .then((dbteams: ITeam[]) => {
                if (dbteams.length === 0) {
                    this.hasMore = false;
                }
                this.setState({
                    teams: [...this.state.teams, ...dbteams], scrollFetching: false
                })
                this.pageNumber++;
            })
    }

    public componentDidMount() {
        this.renderTeamsWithPagination();
        window.addEventListener('scroll', this.onScroll, false);
    }

    public componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll, false);
    }

    public onScroll = () => {
        if ((window.innerHeight + window.scrollY) >= (document.body.offsetHeight - 20) && this.hasMore) {
            if (!this.state.scrollFetching) {
                this.setState({ scrollFetching: true })
                this.renderTeamsWithPagination();
            }
        }
    }

    public opened = (item: ITeam) => {
        if (!this.state.fetching) {
            this.setState({ fetching: true, [item.id]: [], key: item.id });
            item.players.map(this.renderPlayers)
        }
    }

    public renderPlayers = (item: string, index: number) => {
        UserService.getUserById(item)
            .then((user: IUser) => {
                const arr = this.state[this.state.key];
                arr.push(user);
                this.setState({ [this.state.key]: arr, fetching: false })
            })
    }

    public render() {

        const renderPlayers = (element: IUser, i: number) =>
            <li> {`${element.firstName} ${element.lastName} (${element.username}) `}</li>

        const renderTeamItem = (item: ITeam, index: number) => {
            if (!item.isSingleUser) {
                return (

                    <ExpansionPanel CollapseProps={{ mountOnEnter: true }}
                        onClick={() => {
                            this.opened(item)
                        }}>

                        <ExpansionPanelSummary>
                            {` ${item.name} `}
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <p> Members: </p>
                            <ul>
                                {(this.state[item.id] !== undefined) ?
                                    this.state[item.id].map(renderPlayers) : null
                                }
                            </ul>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                );
            }

        }

        return (
            <span>
                <UserHome />
                <div className="teamsLeftPadding">
                    <span>{this.state.teams.map(renderTeamItem)}</span>
                    <AiBot /> 
                    {(this.state.scrollFetching) ? <CircularProgress style={{ margin: "20px" }} /> : null}
                </div>
                               
            </span>

        );
    }
}