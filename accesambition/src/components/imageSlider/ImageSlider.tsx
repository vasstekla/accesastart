import * as React from 'react';
import './ImageSlider.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

class ImageSlider extends React.Component<any, any> {

  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <div className="App">
        <Carousel infiniteLoop autoPlay interval={2000}>
          <div className="imgdiv">
            <img className="img" src={require('../../img/darts.jpg')} />
          </div>
          <div className="imgdiv">
            <img className="img" src={require('../../img/foosball.jpg')} />
          </div>
          <div className="imgdiv">
            <img className="img" src={require('../../img/victory.jpg')} />
          </div>
        </Carousel>
      </div>
    );
  }
}

export default ImageSlider