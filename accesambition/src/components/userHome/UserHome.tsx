import * as React from 'react';
import ToolBar from '../toolbar/Toolbar';
import SideBar from '../sideBar/SideBar';
import AiBot from '../aiBot/aiBot';
import MyProfile from '../myProfile/MyProfile';

export default class UserHome extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
    }

    public render() {
        if (localStorage.getItem('token') != null) {
            return (
                <div>
                    <div>
                        <ToolBar loggedin={true} marginPercent={"210px"} />
                    </div>
                    <div>
                        <SideBar />
                    </div>

                    <AiBot />

                </div>
            );
        }
        else {
            window.location.href = "http://localhost:3000/"
            return (
                <MyProfile />
            );
        }

    }
}