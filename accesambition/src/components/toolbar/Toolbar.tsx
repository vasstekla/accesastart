import * as React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { BrowserRouter as Router } from 'react-router-dom';
import Login from './Login';
import './Toolbar.css';


function ToolBar(props: any) {

  return (
    <Router>
      <div className="grow" style={{ paddingLeft: props.marginPercent }}>
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6" color="inherit" className="center">
              AccesAmbition
            </Typography>

            <Typography color="inherit">
              {props.loggedin ? (
                <button
                  className="button"
                  onClick={() => { localStorage.removeItem('token'); window.location.href = "http://localhost:3000/" }} >Logout</button>
              ) : (
                  <Login />
                )}

            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    </Router>
  );
}

export default ToolBar;