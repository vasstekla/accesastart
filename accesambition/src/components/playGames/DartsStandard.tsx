import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './DartsStandard.css';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Chip from '@material-ui/core/Chip';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { IGameScore } from 'src/models/IGameScore';
import { IGameEntry } from 'src/models/IGameEntry';
import GameService from 'src/services/GameService';
import { ITeamScore } from 'src/models/ITeamScore';
import TeamService from 'src/services/TeamService';
import { ITeam } from 'src/models/ITeam';
import AiBot from '../aiBot/aiBot';

export default class DartsStandard extends React.Component<any, any> {

    private currentPlayerName: string;
    private currentPlayerIndex: number;
    private maxPlayerIndex: number;

    constructor(props: any) {
        super(props);
        this.state = {
            marginPercent: '13.3%',
            teamName: '',
            users: [],
            addedUsers: [],
            addedUsersScore: [],
            addedUser: '',
            selectedUserID: '',
            databaseUsers: [],
            playerSelect: true,
            startScore: 501,
            shotType: 'single',
            shotCount: 0,
            gameEnded: false,
            gameScore: [],
            gameDescription: '',
            uploadedFile: '',
            uploadedFileDisplay: '',
            dartsValues: [[1, 2, 3, 4, 5], [6, 7, 8, 9, 10], [11, 12, 13, 14, 15], [16, 17, 18, 19, 20], [25, 50, 0]]
        }
    }

    public predefinedUsers = (username: string, index: number) => {
        const findValue = this.state.users.find((userItem: ITeam) => userItem.name === username);
        console.log(findValue)
        if (findValue !== undefined) {
            this.handleAddedUsers(findValue.id);
        }
    }

    public componentDidMount() {
        TeamService.getAllTeams()
            .then((dbusers: ITeam[]) => {
                this.setState({
                    users: dbusers,
                    databaseUsers: dbusers.slice()
                })
                if (this.props.location.users !== undefined) {
                    this.props.location.users.map(this.predefinedUsers)
                }
            })
    }

    public validate = () => {
        const addedUsersLength = this.state.addedUsers.length;
        if (addedUsersLength < 2) {
            window.alert("Please select at least 2 players")
        } else {
            const findValue = this.state.databaseUsers.find((userItem: ITeam) => userItem.id === this.state.addedUsers[0])
            this.currentPlayerName = findValue.name;
            this.currentPlayerIndex = 0;
            this.maxPlayerIndex = addedUsersLength;

            const array = this.state.addedUsersScore;
            const gameScoreArray = this.state.gameScore;
            const data: IGameScore = { teamID: '', score: 0, place: 0 };
            for (let i = 0; i < addedUsersLength; i++) {
                array.push(this.state.startScore);
                gameScoreArray.push(data);
            }
            this.setState({ addedUsersScore: array, gameScore: gameScoreArray });
            this.setState({ playerSelect: false })
        }
    }

    public updateGameScore = (item: string, index: number) => {
        let tempScore = 1;
        switch (this.state.gameScore[index].place) {
            case 1: {
                tempScore = 10;
                break;
            }
            case 2: {
                tempScore = 7;
                break;
            }
            case 3: {
                tempScore = 5;
                break;
            }
        }
        const teamScore: ITeamScore = { gameType: 1, score: tempScore };
        TeamService.updateTeamScore(this.state.gameScore[index].teamID, teamScore)
        .then(()=>{
            window.location.href = "http://localhost:3000/history"
        })
    }

    public insertIntoDatabase = () => {
        const gameEntry: IGameEntry = ({
            gameDescription: this.state.gameDescription,
            date: new Date(),
            gameType: 1,
            gameScore: this.state.gameScore,
            image: this.state.uploadedFile
        });

        GameService.insertGame(gameEntry)
            .then(() => {
                this.state.gameScore.map(this.updateGameScore)
            })
    }

    public handleAddedUsers = (value: string) => {
        this.setState({ addedUser: value })
        const array = this.state.addedUsers;
        array.push(value);
        this.setState({ addedUsers: array });

        const arrayUsers = this.state.users;
        const indexUsers = arrayUsers.indexOf(arrayUsers.find((user: ITeam) => user.id === value));
        arrayUsers.splice(indexUsers, 1)
        this.setState({ users: arrayUsers });
    }

    public deleteChip = (user: ITeam) => {

        const array = this.state.addedUsers;
        const index = array.indexOf(array.find((item: string) => item === user.id));
        array.splice(index, 1)
        this.setState({ addedUsers: array });

        const arrayUsers = this.state.users;
        arrayUsers.push(user);
        this.setState({ users: arrayUsers });

    }

    public fillGameScores = () => {
        const temp = JSON.parse(JSON.stringify(this.state.gameScore));
        for (let i = 0; i < this.state.addedUsers.length; i++) {
            temp[i].teamID = this.state.addedUsers[i];
            temp[i].score = this.state.addedUsersScore[i];
        }
        temp.sort((a: IGameScore, b: IGameScore) => {
            return (a.score - b.score);
        })
        for (let i = 0; i < this.state.addedUsers.length; i++) {
            temp[i].place = i + 1;
        }
        this.setState({ gameScore: temp })
    }

    public handleShot = (value: number) => {
        let multiple;
        switch (this.state.shotType) {
            case 'single': {
                multiple = 1;
                break;
            }
            case 'double': {
                multiple = 2;
                break;
            }
            case 'triple': {
                multiple = 3;
                break;
            }
        }

        const array = this.state.addedUsersScore;
        if (array[this.currentPlayerIndex] - (multiple * value) >= 2) {
            array[this.currentPlayerIndex] = array[this.currentPlayerIndex] - (multiple * value);
            this.setState({ addedUsersScore: array });
        }
        else {
            if (array[this.currentPlayerIndex] - (multiple * value) === 0) {
                array[this.currentPlayerIndex] = array[this.currentPlayerIndex] - (multiple * value);
                this.setState({ addedUsersScore: array });
                this.setState({ gameEnded: true })
                this.fillGameScores();
            }
        }

        if (this.state.shotCount === 2) {
            this.setState({ shotCount: 0 })
            if (this.currentPlayerIndex + 1 === this.maxPlayerIndex) {
                this.currentPlayerIndex = 0;
            }
            else {
                this.currentPlayerIndex = this.currentPlayerIndex + 1;
            }
            const findValue = this.state.databaseUsers.find((userItem: ITeam) => userItem.id === this.state.addedUsers[this.currentPlayerIndex])
            this.currentPlayerName = findValue.name;
        }
        else {
            this.setState({ shotCount: this.state.shotCount + 1 })
        }
    }

    public render() {

        const startScoreValues = [
            {
                value: 301
            },
            {
                value: 401
            },
            {
                value: 501
            },
            {
                value: 601
            },
            {
                value: 1001
            },
        ];

        const renderUsers = (item: ITeam, index: number) => {
            if (item.isSingleUser) {
                return (
                    <MenuItem key={item.id} value={item.id}>
                        {`${item.name}`}
                    </MenuItem>
                )
            }
        }

        const renderUsersForTable = (item: string, index: number) => {
            const findValue = this.state.databaseUsers.find((userItem: ITeam) => userItem.id === item)
            return (
                <TableCell>
                    {findValue.name}
                </TableCell>
            );
        }

        const renderUsersForTable2 = (item: IGameScore, index: number) => {
            const findValue = this.state.databaseUsers.find((userItem: ITeam) => userItem.id === item.teamID)
            return (
                <TableCell>
                    {findValue.name}
                </TableCell>
            );
        }

        const renderUsersScoreForTable = (item: number, index: number) =>
            <TableCell align="right">
                {item}
            </TableCell>

        const renderUsersScoreForTable2 = (item: IGameScore, index: number) =>
            <TableCell align="right">
                {item.score}
            </TableCell>

        const renderUsersRankForTable2 = (item: IGameScore, index: number) =>
            <TableCell align="right">
                {item.place}
            </TableCell>

        const renderAddedUsers = (item: string, index: number) => {
            const findValue = this.state.databaseUsers.find((userItem: ITeam) => userItem.id === item)
            return (
                <Chip
                    key={index}                    
                    label={findValue.name}
                    onDelete={() => this.deleteChip(findValue)}
                />
            );
        }

        const renderDartsValues2 = (item: number, index: number) =>
            < td align="right" >
                <button style={{ width: "100px" }} onClick={() => { this.handleShot(item) }}>
                    {item}
                </button>
            </td >


        const renderDartsValues = (item: number[], index: number) =>
            <tr>
                {item.map(renderDartsValues2)}
            </tr>

        return (
            <span className="form" >
                <UserHome />
                {this.state.playerSelect ?
                    <div style={{ marginLeft: this.state.marginPercent }} >
                        <form >
                            <TextField
                                select
                                className="textField"
                                value={this.state.startScore}
                                onChange={(event) => { this.setState({ startScore: event.target.value }) }}
                                helperText="Select start score value"
                                margin="normal"
                            >
                                {startScoreValues.map(option => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.value}
                                    </MenuItem>
                                ))}
                            </TextField>
                            <br />
                            <TextField
                                select
                                className="textField"
                                value={this.state.addedUser}
                                onChange={(event) => { this.handleAddedUsers(event.target.value) }}
                                helperText="Add players"
                                margin="normal"
                            >
                                {this.state.users.map(renderUsers)}
                            </TextField>
                            <br />
                            {this.state.addedUsers.map(renderAddedUsers)}
                            <br />
                            <input
                                type="button"
                                className="button"
                                value="Play"
                                onClick={() => this.validate()}
                            />
                        </form>
                    </div >
                    :
                    <div>
                        {this.state.gameEnded ?
                            <div>
                                <div className="table">
                                    <Table style={{ maxWidth: "400px" }}>
                                        <TableHead>
                                            <TableRow>
                                                {this.state.gameScore.map(renderUsersForTable2)}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                {this.state.gameScore.map(renderUsersScoreForTable2)}
                                            </TableRow>
                                            <TableRow>
                                                {this.state.gameScore.map(renderUsersRankForTable2)}
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </div>
                                <div className="leftPadding">
                                    <form>
                                        <TextField
                                            type="text"
                                            className="textField"
                                            value={this.state.gameDescription}
                                            onChange={(event) => { this.setState({ gameDescription: event.target.value }) }}
                                            helperText="Description"
                                            margin="normal"
                                        />
                                        <br />
                                        <input
                                            type="file"
                                            onChange={(event: any) => {
                                                const reader = new FileReader();

                                                reader.readAsDataURL(event.target.files[0]);
                                                reader.onload = () => {
                                                    this.setState({ uploadedFile: reader.result })
                                                };
                                                this.setState({
                                                    uploadedFileDisplay: URL.createObjectURL(event.target.files[0]),
                                                });
                                            }
                                            }
                                        />
                                        <br />
                                        <img src={this.state.uploadedFileDisplay} />
                                        <br />
                                        <input
                                            type="button"
                                            className="button"
                                            value="Submit game"
                                            onClick={() => this.insertIntoDatabase()}
                                        />
                                    </form>
                                </div>
                            </div>
                            :
                            <div>
                                <div className="table">
                                    <Table style={{ maxWidth: "400px" }}>
                                        <TableHead>
                                            <TableRow>
                                                {this.state.addedUsers.map(renderUsersForTable)}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            <TableRow>
                                                {this.state.addedUsersScore.map(renderUsersScoreForTable)}
                                            </TableRow>
                                        </TableBody>
                                    </Table>
                                </div>
                                <div className="currentPlayer">
                                    <p style={{ textAlign: "center", textAlignLast: "center" }}>
                                        {this.currentPlayerName}
                                    </p>
                                    <form action="">
                                        <input type="radio" name="shotType" value="single" onChange={(event) => { this.setState({ shotType: event.target.value }) }} /> Single
                                        <input type="radio" name="shotType" value="double" onChange={(event) => { this.setState({ shotType: event.target.value }) }} /> Double
                                        <input type="radio" name="shotType" value="triple" onChange={(event) => { this.setState({ shotType: event.target.value }) }} /> Triple
                                    </form>
                                </div>

                                <div className="table2">
                                    <table style={{ maxWidth: "100px" }} className="tablee2">
                                        {this.state.dartsValues.map(renderDartsValues)}
                                    </table>
                                </div>
                            </div>
                        }
                    </div>
                }
                <AiBot />
            </span >

        );
    }
}