import * as React from 'react';
import UserHome from '../userHome/UserHome'
import './Leaderboard.css';
import TeamService from '../../services/TeamService'
import SortByAlpha from '@material-ui/icons/SortByAlpha';
import Sort from '@material-ui/icons/Sort';
import { ITeam } from '../../models/ITeam'
import { ITeamScore } from 'src/models/ITeamScore';
import { Icon } from '@material-ui/core';
import AiBot from '../aiBot/aiBot';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export default class Leaderboard extends React.Component<any, any> {

    private tempAllScore: number = 0;

    constructor(props: any) {
        super(props);
        this.state = {
            teams: [],
            fetching: false,
            sortedTeamAlphabetically: false,
            sortedDartsScore: false,
            sortedFoosballScore: false,
            sortedAllScore: false
        }
    }

    public componentWillMount() {
        TeamService.getAllTeams()
            .then((dbteams: ITeam[]) => {
                this.setState({ teams: dbteams })
            })

    }

    public renderTeamScore = (team: ITeam, index: number) => {
        this.tempAllScore = 0;
        return (
            <TableRow >
                <TableCell> {team.name} </TableCell>
                {team.scores.map(this.renderTeamScores)}
                <TableCell> {this.tempAllScore} </TableCell>
            </TableRow >
        )
    }

    public renderTeamScores = (teamScore: ITeamScore, index: number) => {
        this.tempAllScore = this.tempAllScore + teamScore.score;
        return (
            <TableCell>
                {teamScore.score}
            </TableCell>
        )
    }

    public sortTeams = () => {
        const arr = this.state.teams;
        let sort = false;
        if (!this.state.sortedTeamAlphabetically) {
            arr.sort((a: ITeam, b: ITeam) => {
                return (a.name.localeCompare(b.name));
            })
            sort = true;
        } else {
            arr.sort((a: ITeam, b: ITeam) => {
                return (b.name.localeCompare(a.name));
            })
        }
        this.setState({ teams: arr, sortedTeamAlphabetically: sort })
    }

    public sortScore = (gameType: number) => {
        const arr = this.state.teams;
        let sort = false;
        let sortedScore;
        let sortedScoreState;

        switch (gameType) {
            case 0: {
                sortedScore = this.state.sortedDartsScore
                sortedScoreState = "sortedDartsScore";
            }
            case 1: {
                sortedScore = this.state.sortedFoosballScore
                sortedScoreState = "sortedFoosballScore";
            }
        }


        if (!sortedScore) {
            arr.sort((a: ITeam, b: ITeam) => {
                return (a.scores[gameType].score - b.scores[gameType].score);
            })
            sort = true;
        } else {
            arr.sort((a: ITeam, b: ITeam) => {
                return (b.scores[gameType].score - a.scores[gameType].score);
            })
        }
        this.setState({ teams: arr, [sortedScoreState]: sort })
    }

    public sortAllScore = () => {
        const arr = this.state.teams;
        let sort = false;
        if (!this.state.sortedAllScore) {
            arr.sort((a: ITeam, b: ITeam) => {
                return ((a.scores[0].score + a.scores[1].score) - (b.scores[0].score + b.scores[1].score));
            })
            sort = true;
        } else {
            arr.sort((a: ITeam, b: ITeam) => {
                return ((b.scores[0].score + b.scores[1].score) - (a.scores[0].score + a.scores[1].score));
            })
        }
        this.setState({ teams: arr, sortedAllScore: sort })
    }

    public render() {

        return (
            <span className="center">
                <UserHome />
                <div className="left">
                    <Table className="table">
                        <TableHead>
                        <TableRow>
                            <TableCell onClick={this.sortTeams}> Team <Icon><SortByAlpha /></Icon></TableCell>
                            <TableCell onClick={() => { this.sortScore(0) }}> Darts score <Icon><Sort /></Icon></TableCell>
                            <TableCell onClick={() => { this.sortScore(1) }}> Foosball score<Icon><Sort /></Icon></TableCell>
                            <TableCell onClick={this.sortAllScore}> All score<Icon><Sort /></Icon></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                        {this.state.teams.map(this.renderTeamScore)}
                        </TableBody>
                    </Table>
                </div>
                <AiBot />    
            </span>

        );
    }
}