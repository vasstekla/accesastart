import * as React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountBox from '@material-ui/icons/AccountBox';
import VideogameAsset from '@material-ui/icons/VideogameAsset';
import Group from '@material-ui/icons/Group';
import { Link } from 'react-router-dom';
import { Collapse } from '@material-ui/core';
import History from '@material-ui/icons/History';
import GroupAdd from '@material-ui/icons/GroupAdd';
import GroupWork from '@material-ui/icons/GroupWork';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import PlayArrow from '@material-ui/icons/PlayArrow';
import ListAlt from '@material-ui/icons/ListAlt';
import Add from '@material-ui/icons/Add';
import './SideBar.css';


export default class SideBar extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            gamesOpen: true,
            teamsOpen: true,
            playOpen: true,
            dartsOpen: false,
        }
    }

    public handleGamesClick = () => {
        this.setState(state => ({ gamesOpen: !state.gamesOpen }));
    };

    public handleTeamsClick = () => {
        this.setState(state => ({ teamsOpen: !state.teamsOpen }));
    };

    public handlePlayClick = () => {
        this.setState(state => ({ playOpen: !state.playOpen }));
    };

    public handleDartsClick = () => {
        this.setState(state => ({ dartsOpen: !state.dartsOpen }));
    }

    public render() {
        return (
            <Drawer variant="permanent" className="drawer">
                <List>
                    <Link to="/profile">
                        <ListItem button key={"My Profile"} >
                            <ListItemIcon>
                                <AccountBox />
                            </ListItemIcon>
                            <ListItemText primary={"My Profile"} />
                        </ListItem>
                    </Link>

                    <ListItem button onClick={this.handleGamesClick} >
                        <ListItemIcon>
                            <VideogameAsset />
                        </ListItemIcon>
                        <ListItemText primary={"Games"} />
                        {this.state.open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem >
                    <div className="nested">
                        <Collapse in={this.state.gamesOpen} className="nested" timeout="auto" unmountOnExit>
                            <List>
                                <Link to="/history">
                                    <ListItem button key={"History"} >
                                        <ListItemIcon>
                                            <History />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"History"} />
                                    </ListItem>
                                </Link>
                                <Link to="/newGame">
                                    <ListItem button key={"New Game"}>
                                        <ListItemIcon>
                                            <Add />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"New Game"} />
                                    </ListItem>
                                </Link>
                            </List>
                        </Collapse>
                    </div>

                    <ListItem button onClick={this.handlePlayClick} >
                        <ListItemIcon>
                            <PlayArrow />
                        </ListItemIcon>
                        <ListItemText primary={"Play"} />
                        {this.state.open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem >
                    <div className="nested">
                        <Collapse in={this.state.playOpen} className="nested" timeout="auto" unmountOnExit>
                            <List>
                                <Link to="/foosball">
                                    <ListItem button key={"Foosball"} >
                                        <ListItemIcon>
                                            <PlayArrow />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"Foosball"} />
                                    </ListItem>
                                </Link>
                                <Link to="/dartsStandard">
                                    <ListItem button key={"Darts"} >
                                        <ListItemIcon>
                                            <PlayArrow />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"Darts"} />
                                    </ListItem>
                                </Link>
                            </List>
                        </Collapse>
                    </div>
                    <ListItem button onClick={this.handleTeamsClick} >
                        <ListItemIcon>
                            <Group />
                        </ListItemIcon>
                        <ListItemText primary={"Teams"} />
                        {this.state.open ? <ExpandLess /> : <ExpandMore />}
                    </ListItem >
                    <div className="nested">
                        <Collapse in={this.state.teamsOpen} className="nested" timeout="auto" unmountOnExit>
                            <List>
                                <Link to="/teams">
                                    <ListItem button key={"All teams"} >
                                        <ListItemIcon>
                                            <GroupWork />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"All teams"} />
                                    </ListItem>
                                </Link>
                                <Link to="/newTeam">
                                    <ListItem button key={"New Team"}>
                                        <ListItemIcon>
                                            <GroupAdd />
                                        </ListItemIcon>
                                        <ListItemText inset primary={"New Team"} />
                                    </ListItem>
                                </Link>
                            </List>
                        </Collapse>
                    </div>

                    <Link to="/leaderboard" >
                        <ListItem button key={"Leaderboard"}>
                            <ListItemIcon>
                                <ListAlt />
                            </ListItemIcon>
                            <ListItemText primary={"Leaderboard"} />
                        </ListItem>
                    </Link>
                </List>
            </Drawer>
        );
    }
}