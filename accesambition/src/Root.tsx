import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './components/home/Home'
import UserHome from './components/userHome/UserHome'
import MyProfile from './components/myProfile/MyProfile';
import AllTeams from './components/teams/AllTeams';
import NewTeam from './components/teams/NewTeam';
import GameHistory from './components/games/GameHistory';
import NewGame from './components/games/NewGame';
import Leaderboard from './components/leaderboard/Leaderboard';
import Foosball from './components/playGames/Foosball';
import DartsStandard from './components/playGames/DartsStandard';

const Root = () => (
    <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/user" component={UserHome} />
        <Route path="/profile" component={MyProfile} />        
        <Route path="/history" component={GameHistory} />
        <Route path="/newGame" component={NewGame} />
        <Route path="/foosball" component={Foosball} />
        <Route path="/dartsStandard" component={DartsStandard} />
        <Route path="/teams" component={AllTeams} />
        <Route path="/newTeam" component={NewTeam} />
        <Route path="/leaderboard" component={Leaderboard} />
    </Switch>
)

export default Root;