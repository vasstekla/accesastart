﻿using Gamification.DataModel;
using Gamification.Model;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Context
{
    public interface IContext
    {
        IMongoCollection<Game> GameCollection { get; }

        IMongoCollection<User> UserCollection { get; }

        IMongoCollection<Team> TeamCollection { get; }
    }
}
