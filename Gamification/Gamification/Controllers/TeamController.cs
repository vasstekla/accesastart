﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Gamification.DataModel;
using Gamification.Model;
using Gamification.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Gamification.Controllers
{

    [ApiController]
    [Route("api/team")]
    public class TeamController : ControllerBase
    {
        private readonly ITeamRepository _teamRepository;

        public TeamController(ITeamRepository teamRepository)
        {
            _teamRepository = teamRepository;
        }
        [HttpGet]
        [Route("getAll")]
        public async Task<IActionResult> Get()
        {
            return new ObjectResult(await _teamRepository.GetAllTeams());
        }

        [HttpGet]
        [Route("getAllPagination")]
        public async Task<IActionResult> GetAllPasignation([FromQuery(Name = "pageSize")] string pageSize, [FromQuery(Name = "pageNumber")] string pageNumber)
        {
            return new ObjectResult(await _teamRepository.GetAllTeamsWithPaginate(Int32.Parse(pageSize), Int32.Parse(pageNumber)));
        }

        [HttpGet]        
        public async Task<IActionResult> Get([FromQuery(Name = "teamId")] string Id)
        {
            var getid = ObjectId.Parse(Id);
            return new ObjectResult(await _teamRepository.GetById(getid));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Team team)
        {
            await _teamRepository.Insert(team);
            return new OkObjectResult(team);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]TeamScore teamScore)
        {
            var getid = ObjectId.Parse(id);            
            return new OkObjectResult(await _teamRepository.UpdateScore(getid, teamScore));
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromQuery(Name = "teamName")] string teamName, [FromQuery(Name = "newName")] string newName)
        {
            return new OkObjectResult(await _teamRepository.UpdateName(teamName, newName));
        }
    }
}
