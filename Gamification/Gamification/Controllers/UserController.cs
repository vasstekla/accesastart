﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Gamification.Context;
using Gamification.DataModel;
using Gamification.Model;
using Gamification.Repository;

using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;


namespace Gamification.Controllers
{

    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly Authenthific authenthific = new Authenthific();

        public UserController(IUserRepository userRepository, ITeamRepository teamRepository)
        {
            _userRepository = userRepository;
            _teamRepository = teamRepository;
        }

        // GET: api/users
        [HttpGet]
        [Route("api/users")]
        public async Task<IActionResult> Get()
        {
            return new ObjectResult(await _userRepository.GetAllUser());
        }

        [HttpGet]
        [Route("api/user")]
        public async Task<IActionResult> Get([FromQuery(Name = "id")] string Id)
        {
            var getid = ObjectId.Parse(Id);
            return new ObjectResult(await _userRepository.GetById(getid));
        }

        [HttpPost]
        [Route("api/user")]
        public async Task<IActionResult> Post([FromBody]User user)
        {
            LinkedList<TeamScore> scores = new LinkedList<TeamScore>();
            scores.AddLast(new TeamScore { GameType = GameType.Darts, Score = 0 });
            scores.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 0 });

            user.Password = authenthific.HashPassword(user.Password);
            await _userRepository.Insert(user);
            await _teamRepository.Insert(user.Username, user.Id.ToString(), scores, true);
            return new OkObjectResult(user);
        }

        [HttpPost]
        [Route("api/users/login")]
        public async Task<IActionResult> Athenticate([FromBody]User user)
        {
            User tempUser = await _userRepository.GetByUsername(user.Username);
            if (user == null)
                return new ObjectResult(Task.FromResult<User>(null));

            string savedPasswordHash = tempUser.Password;
            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);
            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);

            var pbkdf2 = new Rfc2898DeriveBytes(user.Password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            for (int i = 0; i < 20; i++)
                if (hashBytes[i + 16] != hash[i])
                    return new ObjectResult(Task.FromResult<User>(null));
           

            return new ObjectResult(await _userRepository.Authenticate(tempUser));
        }

        [HttpPut]
        [Route("api/user/{id}")]
        public async Task<IActionResult> Put(string id, [FromBody]User user)
        {
            var getid = ObjectId.Parse(id);
            User tempUser = await _userRepository.GetById(getid);
            if (user.Password == "")
            {
                user.Password = tempUser.Password;
            }
            else
            {
                user.Password = authenthific.HashPassword(user.Password);
            }
            return new OkObjectResult(await _userRepository.Update(getid, user));
        }

        
    }
}
