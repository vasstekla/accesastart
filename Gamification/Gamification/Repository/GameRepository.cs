﻿using Gamification.Context;
using Gamification.DataLayer;
using Gamification.DataModel;
using Gamification.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Gamification.Repository
{
    public class GameRepository : IGameRepository
    {
        private readonly IContext _context;
       
        public GameRepository(IContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Game>> GetAllGames()
        {
            return await _context.GameCollection.Find(_ => true).ToListAsync();
        }

        public async Task Insert(Game game)
        {
            await _context.GameCollection.InsertOneAsync(game);
        }
    }
}
