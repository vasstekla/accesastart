﻿using Gamification.DataModel;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.DataLayer
{
    public interface IGameRepository
    {
        Task Insert(Game game);

        Task<IEnumerable<Game>> GetAllGames();

    }
}
