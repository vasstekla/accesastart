﻿using Gamification.Context;
using Gamification.DataLayer;
using Gamification.DataModel;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Gamification.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly IContext _context;
        private readonly AppSettings _appSettings;

        public UserRepository(IContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public Task<User> Authenticate(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            return Task.FromResult < User > (user);

        }

        public async Task<bool> Delete(ObjectId Id)
        {
            var filter = Builders<User>.Filter.Eq<ObjectId>(u => u.Id, Id);
            DeleteResult deleteResult = await _context.UserCollection.DeleteOneAsync(filter);
            return deleteResult.IsAcknowledged
                && deleteResult.DeletedCount > 0;
        }

        public async Task<IEnumerable<User>> GetAllUser()
        {
            return await _context.UserCollection.Find(_ => true).ToListAsync();
        }

        public async Task<User> GetById(ObjectId Id)
        {
            var filter = Builders<User>.Filter.Eq<ObjectId>(u => u.Id, Id);
            return await _context.UserCollection.FindSync(filter).FirstOrDefaultAsync();
        }

        public async Task<User> GetByUsername(string username)
        {
            var filter = Builders<User>.Filter.Eq<String>(u => u.Username, username);
            return await _context.UserCollection.FindSync(filter).FirstOrDefaultAsync();
        }


        public async Task Insert(User user)
        {
            await _context.UserCollection.InsertOneAsync(user);
        }

        public async Task<bool> Update(ObjectId Id, User user)
        {          
            var filter = Builders<User>.Filter.Eq<ObjectId>(u => u.Id, Id);
            user.Id = Id;
            

            ReplaceOneResult updateResult = _context.UserCollection.ReplaceOne(filter, user);
            return updateResult.IsAcknowledged && updateResult.ModifiedCount > 0;
        }

        
    }
}
