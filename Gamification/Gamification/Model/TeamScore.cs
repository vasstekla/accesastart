﻿using Gamification.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Model
{
    public class TeamScore
    {
        public GameType GameType { get; set; }
        public int Score { get; set; }
    }
}
