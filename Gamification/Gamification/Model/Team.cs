﻿using Gamification.Model;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.DataModel
{
    public class Team
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public String Name { get; set; }
        public LinkedList<ObjectId> Players { get; set; }
        public LinkedList<TeamScore> Scores { get; set; }
        public Boolean IsSingleUser { get; set; }

        [JsonConstructor]
        public Team(string name, LinkedList<String> players, LinkedList<TeamScore> scores, Boolean isSingleUser)
        {
            LinkedList<ObjectId> tempPlayers = new LinkedList<ObjectId>();
            foreach (String player in players)
            {
                ObjectId getid = ObjectId.Parse(player);
                tempPlayers.AddLast(getid);
            }

            Name = name;
            Players = tempPlayers;
            Scores = scores;
            IsSingleUser = isSingleUser;            

        }

        public Team(string name, LinkedList<ObjectId> players, LinkedList<TeamScore> scores, Boolean isSingleUser)
        {
            Name = name;
            Players = players;
            Scores = scores;
            IsSingleUser = isSingleUser;

        }
    }
}
