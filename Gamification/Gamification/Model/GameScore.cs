﻿using Gamification.DataModel;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.Model
{
    public class GameScore
    {
        public ObjectId TeamID { get; set; }
        public int Score { get; set; }
        public int Place { get; set; }

        [JsonConstructor]
        public GameScore(string teamId, int score, int place)
        {         
            TeamID = ObjectId.Parse(teamId);
            Score = score;
            Place = place;
        }

        public GameScore(ObjectId teamId, int score, int place)
        {
            TeamID = teamId;
            Score = score;
            Place = place;
        }
    }
}
