﻿using Gamification.Model;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gamification.DataModel
{
    public class Game
    {
        [BsonId]
        public ObjectId Id { get; set; }
        public String GameDescription { get; set; }
        public DateTime Date { get; set; }
        public GameType GameType { get; set; }
        public LinkedList<GameScore> GameScore { get; set; }
        public String Image { get; set; }

    }
}
