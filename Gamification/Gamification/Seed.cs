﻿using System;
using System.Collections.Generic;
using Gamification.DataModel;
using MongoDB.Bson;
using MongoDB.Driver;
using Gamification.Context;
using Gamification.Model;
using System.Security.Cryptography;
using Gamification.Repository;

namespace Gamification
{
    public class Seed
    {
        private IMongoDatabase database;
        private IMongoCollection<User> userCollection;
        private IMongoCollection<Team> teamCollection;
        private IMongoCollection<Game> gamesCollection;

        private GameType type;

        public async System.Threading.Tasks.Task SeedTheDatabaseAsync()
        {
            ConnectToDatabase();
            DropExistingCollections();
            CreateCollections();

            String savedPasswordHash;

            savedPasswordHash = HashPassword("banana");
            userCollection.InsertOne(new User("John", "Smith", "banana", "banana@accesa.com", savedPasswordHash, "", ""));

            LinkedList<TeamScore> scores = new LinkedList<TeamScore>();
            scores.AddLast(new TeamScore { GameType = GameType.Darts, Score = 0 });
            scores.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 0 });

            LinkedList<TeamScore> scoress = new LinkedList<TeamScore>();
            scoress.AddLast(new TeamScore { GameType = GameType.Darts, Score = 41 });
            scoress.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 115 });

            var userFilter = Builders<User>.Filter.Eq<String>(u => u.Username, "banana");
            LinkedList<ObjectId> players = new LinkedList<ObjectId>();
            players.AddLast(userCollection.FindSync(userFilter).FirstOrDefaultAsync().Result.Id);
            await teamCollection.InsertOneAsync(new Team("banana", players, scoress, true));

            savedPasswordHash = HashPassword("apple");
            userCollection.InsertOne(new User("Sarah", "Johnson", "apple", "apple@accesa.com", savedPasswordHash, "", ""));
            
            LinkedList<TeamScore> scores1 = new LinkedList<TeamScore>();
            scores1.AddLast(new TeamScore { GameType = GameType.Darts, Score = 214 });
            scores1.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 7 });

            var userFilter1 = Builders<User>.Filter.Eq<String>(u => u.Username, "apple");
            LinkedList<ObjectId> players1 = new LinkedList<ObjectId>();
            players1.AddLast(userCollection.FindSync(userFilter1).FirstOrDefaultAsync().Result.Id);
            await teamCollection.InsertOneAsync(new Team("apple", players1, scores1, true));

            savedPasswordHash = HashPassword("orange");
            userCollection.InsertOne(new User("Darth", "Vader", "orange", "orange@accesa.com", savedPasswordHash, "", ""));

            LinkedList<TeamScore> scores2 = new LinkedList<TeamScore>();
            scores2.AddLast(new TeamScore { GameType = GameType.Darts, Score = 10 });
            scores2.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 54 });

            var userFilter2 = Builders<User>.Filter.Eq<String>(u => u.Username, "orange");
            LinkedList<ObjectId> players2 = new LinkedList<ObjectId>();
            players2.AddLast(userCollection.FindSync(userFilter2).FirstOrDefaultAsync().Result.Id);
            await teamCollection.InsertOneAsync(new Team("orange", players2, scores2, true));

            savedPasswordHash = HashPassword("honey");
            userCollection.InsertOne(new User("Freddie", "Mercury", "honey", "honey@accesa.com", savedPasswordHash, "", ""));

            LinkedList<TeamScore> scores3 = new LinkedList<TeamScore>();
            scores3.AddLast(new TeamScore { GameType = GameType.Darts, Score = 25 });
            scores3.AddLast(new TeamScore { GameType = GameType.Foosball, Score = 11 });

            var userFilter3 = Builders<User>.Filter.Eq<String>(u => u.Username, "honey");
            LinkedList<ObjectId> players3 = new LinkedList<ObjectId>();
            players3.AddLast(userCollection.FindSync(userFilter3).FirstOrDefaultAsync().Result.Id);
            await teamCollection.InsertOneAsync(new Team("honey", players3, scores3, true));

            LinkedList<ObjectId> playerss = new LinkedList<ObjectId>();
            playerss.AddLast(userCollection.FindSync(userFilter3).FirstOrDefaultAsync().Result.Id);
            playerss.AddLast(userCollection.FindSync(userFilter2).FirstOrDefaultAsync().Result.Id);

            teamCollection.InsertOne(new Team("Legends1", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends2", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends3", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends4", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends5", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends6", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends7", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends8", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends9", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends10", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends11", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends12", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends13", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends14", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends15", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends16", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends17", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends18", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends19", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends20", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends21", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends22", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends23", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends24", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends25", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends26", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends27", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends28", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends29", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends30", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends31", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends32", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends33", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends34", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends35", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends36", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends37", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends38", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends39", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends40", playerss, scores, false));


            teamCollection.InsertOne(new Team("Legends", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends", playerss, scores, false));
            teamCollection.InsertOne(new Team("Legends", playerss, scores, false));

            LinkedList<ObjectId> playerss2 = new LinkedList<ObjectId>();
            playerss2.AddLast(userCollection.FindSync(userFilter1).FirstOrDefaultAsync().Result.Id);
            playerss2.AddLast(userCollection.FindSync(userFilter).FirstOrDefaultAsync().Result.Id);

            teamCollection.InsertOne(new Team("Ordinary", playerss2, scores, false));

            teamCollection.InsertOne(new Team("Someone", playerss2, scores, false));

            var filter5 = Builders<Team>.Filter.Eq<String>(u => u.Name, "Legends");
            LinkedList<GameScore> teamScores = new LinkedList<GameScore>();
            teamScores.AddLast(new GameScore(teamCollection.FindSync(filter5).FirstOrDefaultAsync().Result.Id, 150, 1));

            var filter6 = Builders<Team>.Filter.Eq<String>(u => u.Name, "Ordinary");
            teamScores.AddLast(new GameScore(teamCollection.FindSync(filter6).FirstOrDefaultAsync().Result.Id, 72, 2));

            gamesCollection.InsertOne(new Game
            {
                GameDescription = "Best game ever",
                Date = GenerateRandomDate(),
                GameType = GameType.Darts,
                GameScore = teamScores
            });

            var filter7 = Builders<Team>.Filter.Eq<String>(u => u.Name, "Someone");
            teamScores.AddLast(new GameScore(teamCollection.FindSync(filter7).FirstOrDefaultAsync().Result.Id, 25, 3));
            System.Diagnostics.Debug.WriteLine(teamScores);
            gamesCollection.InsertOne(new Game
            {
                GameDescription = "This was so damn epic",
                Date = GenerateRandomDate(),
                GameType = GameType.Foosball,
                GameScore = teamScores
            });
        }

        private static DateTime GenerateRandomDate()
        {
            Random random = new Random();
            DateTime start = new DateTime(2000, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(random.Next(range));
        }

        public void ConnectToDatabase()
        {
            var db = new MongoClient();
            database = db.GetDatabase("database");
        }

        public void DropExistingCollections()
        {
            database.DropCollection("users");
            database.DropCollection("teams");
            database.DropCollection("games");
            database.DropCollection("leaderboard");
        }

        public void CreateCollections()
        {
            userCollection = database.GetCollection<User>("users");
            teamCollection = database.GetCollection<Team>("teams");
            gamesCollection = database.GetCollection<Game>("games");
        }

        public String HashPassword(String password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            String savedPasswordHash = Convert.ToBase64String(hashBytes);

            return savedPasswordHash;
        }

    }
}
